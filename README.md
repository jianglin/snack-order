# Struct 
## com.doorgga.config
	* SpringDBConfig.java : Setup hsqldb to store data
	* SpringWebConfig.java : Configure the resources, JSP locations, etc.

## com.doorgga.controller
	* SnackController : controller to dispatch url request

## com.doorgga.dao
	* SnackDao.java : DAO interface
	* SnackDaoImp.java : DAO implementation
	
## com.doorgga.domain
	* Snack.java : snack object that store the snack info

## com.doorgga.service
	* CookieService.java : service interface which handles all operation related to CookieService
	* CookieServiceImp.java : cookie service implementation
	* SnackService.java : service interface which handles snack info operation
	* SnackServiceImp.java : snack service implementation

## com.doorgga.validator
	* SuggestedSnackValidator.java : suggested snack input validator

## resources
	* config.properties : api url and key
## resources/db/sql
	* create-db.sql : Create table to store Votes info
## resources/message : 
	* validation.properties : validation error messages

# The webapp logic: #
* Starts at /voting, check cookieMonth to see if cookie stored month is the same as server month, otherwise, reset cookie.  Get userId, check the remaining number of votes.  If no votes remained, disable all vote checkboxes.  getAllSnacks() from webservice, store all snacks info in allSnackList, filter non-optional and 'suggested' snack list and 'voted' snacks for this user, disable checkbox which has been voted, then render in VotingPage.jsp.

* When user vote for a snack, update the Votes(Any snacks in this table have been suggested) and Votes_Detail(Store voted snacks for this user) tables. And also update Snack with 'hasVoted' field in allSnackList.

* At suggestion page, if this user has suggested any snacks, disable the suggest button. User selects a snack or requests webapi to get a new snack, insert vote value 0 for this snack in Votes table, so the voting page will get all suggested snacks if it finds records in Votes table. If user suggests a new snack by posting JSON to webapi or selects a existing snack, allSnackList will be updated by calling getAllSnack() again to refresh the list, updates 'vote' and 'hasSuggested' filed of the Snack object in allSnackList.  

* When navigate to shopping list. access allSnackList to find the non-optional Snacks along with the optional Snacks with higher votes.
 

# Import #

* Use Spring Eclipse with Maven plugin  https://spring.io/tools/eclipse

* Import -> Existing Maven Projects -> Select THIS folder -> Advanced -> Name template to be [artifactId].[version] -> check /pom.xml

* Then run on server 

* Go to http://localhost:8080/PROJECTNAME/index to launch the webapp.