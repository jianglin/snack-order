package com.doorgga.controller;

import java.util.Calendar;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.doorgga.domain.Snack;
import com.doorgga.service.CookieService;
import com.doorgga.service.SnackService;
import com.doorgga.validator.SuggestedSnackValidator;




@Controller
@SessionAttributes({ "allsnacks" })

public class SnackController {

	private final Logger logger = LoggerFactory.getLogger(SnackController.class);

	@Autowired
	private SuggestedSnackValidator suggestedSnackValidator;
	
	@InitBinder("suggestedsnack")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(suggestedSnackValidator);
	}
	
	@Autowired
	private SnackService snackService;

	@Autowired
	private CookieService cookieService;

	@ModelAttribute("allsnacks")
	public List<Snack> getSnackList() {
		return snackService.getAllSnacks();
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(Model model) {
		logger.debug("index()");
		return "redirect:/voting";
	}

	/*
	 * Update snack list once a new snack has been suggested
	 */
	@RequestMapping(value = "/updateSnackList", method = RequestMethod.GET)
	public String updateSnackList(@ModelAttribute("allsnacks") List<Snack> allSnackList, Model model) {
		logger.debug("updateSnackList()");
		allSnackList = snackService.getAllSnacks();
		model.addAttribute("allsnacks", allSnackList);
		return "redirect:/voting";
	}

	/*
	 * Voting page. 
	 * 1. Check if cookieMonth is current month, if not reset numOfVotes and hasSuggested cookie 
	 * 2. Check numOfVotes remaining 
	 * 3. Update votes and suggestion data for all snacks 
	 * 4. Filter allwaysPurchasedSnackList and suggestedSnackList for display. 
	 */
	@RequestMapping(value = "/voting", method = RequestMethod.GET)
	public String voting(@ModelAttribute("allsnacks") List<Snack> allSnackList, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		logger.debug("Voting()");

		cookieService.setHttpServletRequest(request);
		cookieService.setHttpServletResponse(response);
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		String currentMonth = "" + year + month;
		String cookieMonth = cookieService.getCookieMonth();
		String userId = cookieService.getUserId();
		if (userId.length() == 0) {
			userId = cookieService.setUserId();
		}
		int numOfVotes = cookieService.getNumOfVotes(); 
		if (currentMonth.equals(cookieMonth)) {
			if (numOfVotes <= 0) {
				// TODO disable checkboxes
				logger.debug("No votes remaining for " + userId + " in "+ cookieMonth);
				model.addAttribute("maxVoteReached", "");
				model.addAttribute("voteCheckboxDisabled", "disabled");
				model.addAttribute("voteCheckbox", "icon-check_noVote");
			} else {
				logger.debug(numOfVotes + " Votes Remaining for " + userId + " in "+ cookieMonth);
				model.addAttribute("maxVoteReached", "isHidden");
				model.addAttribute("voteCheckboxDisabled", "");
				model.addAttribute("voteCheckbox", "icon-check_voted");

			}
		} else {
			cookieService.resetCookies();
			cookieService.setCookieMonth();
		}
		snackService.updateSnackVotes(allSnackList);
		model.addAttribute("voteRemaining", numOfVotes);
		model.addAttribute("hidden1", "isHidden");
		model.addAttribute("hidden2", "isHidden");
		model.addAttribute("hidden3", "isHidden");
		model.addAttribute("hidden" + numOfVotes, "");
		
		List<Integer> votedSnackIds = snackService.getVotedSnackIds(userId);
		List<Snack> suggestedSnackList = snackService.getSuggestedSnacks(allSnackList, votedSnackIds);
		model.addAttribute("allwaysPurchasedSnackList", snackService.getTopNSnacks(allSnackList, 2));
		model.addAttribute("suggestedSnackList", suggestedSnackList);
		return "VotingPage";

	}

	/*
	 * Voting for specific Snack id 
	 * Update vote data
	 */
	@RequestMapping(value = "/voting/{id}", method = RequestMethod.POST)
	public String voteForSnack(@PathVariable("id") int id, @ModelAttribute("allsnacks") List<Snack> allSnackList,
			Model model, final RedirectAttributes redirectAttributes, HttpServletRequest request,
			HttpServletResponse response) {
		logger.debug("voting() id: {}", id);
		cookieService.setHttpServletRequest(request);
		cookieService.setHttpServletResponse(response);
		String userId = cookieService.getUserId();
		Snack s = new Snack(id);
		if (!snackService.insertVoteDetail(userId, s)) {
			redirectAttributes.addFlashAttribute("msg", "Error on /voting/"+id + " (Has voted for this snack)");
			return "redirect:/error";
		}
		if (snackService.updateSnackVote(allSnackList, s)) {
			cookieService.decreaseNumOfVotes();
			return "redirect:/voting";
		}
		redirectAttributes.addFlashAttribute("msg", "Error on /voting/"+id);
		return "redirect:/error";
	}

	/*
	 * suggestion page 
	 * 1. Load optionalSnackList for dropdown box 
	 * 2. Binding suggestedsnack to suggestion form data
	 */
	@RequestMapping(value = "/suggestions", method = RequestMethod.GET)
	public String getSuggestions(@ModelAttribute("allsnacks") List<Snack> allSnackList, Model model,
			final RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Suggestions()");

		Snack suggestedSnack = new Snack();
		List<Snack> optionalSnackList = snackService.getOptionalSnacks(allSnackList);
		model.addAttribute("suggestedsnack", suggestedSnack);
		model.addAttribute("optionalSnackList", optionalSnackList);
		model.addAttribute("suggestionIncomplete", "isHidden");
		cookieService.setHttpServletRequest(request);
		cookieService.setHttpServletResponse(response);
		if (cookieService.getHasSuggested()) {
			model.addAttribute("suggestionHasBeenMade", "");
			model.addAttribute("suggestionButtonDisabled", "disabled");
		} else {
			model.addAttribute("suggestionHasBeenMade", "isHidden");
		}
		return "SuggestionsPage";

	}


	/*
	 *  Get selected or new snack.
	 *  1. Check suggestedSnack, if id = -1, get suggestedSnack binding data to get Snack from web service
	 *  2. Update db, insert vote 0 for this snack. 
	 */
	@RequestMapping(value = "/suggestions", method = RequestMethod.POST)
	public String suggestionForSnack(@ModelAttribute("allsnacks") List<Snack> allSnackList,
			@ModelAttribute("suggestedsnack") @Validated Snack suggestedSnack, BindingResult result, Model model,
			final RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) {
		logger.debug("suggested {},{},{}", suggestedSnack.getId(), suggestedSnack.getName(),
				suggestedSnack.getPurchaseLocations());

		Snack s = new Snack(suggestedSnack.getId());
		if (suggestedSnack.getId() == -1) {
			if ( result.hasErrors() ) {
				List<Snack> optionalSnackList = snackService.getOptionalSnacks(allSnackList);
				model.addAttribute("optionalSnackList", optionalSnackList);
				model.addAttribute("suggestedsnack", suggestedSnack);
				model.addAttribute("suggestionHasBeenMade", "isHidden");
				model.addAttribute("suggestionIncomplete", "");
				return "SuggestionsPage";
			} else  {
				s = snackService.getNewSuggestedSnack(suggestedSnack);
				//TODO check if getNewSuggestedSnack returns error, if has error show error message
				allSnackList.add(s);
			}
		} 
		cookieService.setHttpServletRequest(request);
		cookieService.setHttpServletResponse(response);
		
		
		if (snackService.getSnackVote(s.getId()) >= 0) {
			redirectAttributes.addFlashAttribute("suggestionAlreadyExist", "true");
			return "redirect:/suggestions";
		}

		if (snackService.updateSnackVote(allSnackList, s)) {
			cookieService.setHasSuggested(true);
			return "redirect:/updateSnackList";
		} else {
			redirectAttributes.addFlashAttribute("msg", "Error on /suggestions/suggesting for Snack id=" +s.getId() );
			return "redirect:/error";
		}
	}

	/*
	 * List shopping snacks
	 * Get all non-optional snacks and (10 - number of nonoptional) optional snacks order by votes
	 */
	@RequestMapping(value = "/shoppinglist", method = RequestMethod.GET)
	public String shoppingList(@ModelAttribute("allsnacks") List<Snack> allSnackList, Model model) {
		logger.debug("shopping list()");
		snackService.updateSnackVotes(allSnackList);
		List<Snack> shoppingSnackList = snackService.getTopNSnacks(allSnackList, 10);
		model.addAttribute("shoppingSnackList", shoppingSnackList);
		return "ShoppingListPage";
	}

	/*
	 * Error page
	 * Show error message
	 */
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String error(Model model) {
		logger.debug("error()");
		return "ErrorPage";
	}

	/*
	 * Show cookie
	 */
	@RequestMapping(value = "/cookie", method = RequestMethod.GET)
	public String showCookie(Model model, HttpServletRequest request, HttpServletResponse response) {
		logger.debug("show cookie()");
		cookieService.setHttpServletRequest(request);
		cookieService.setHttpServletResponse(response);
		String cookies = "userId: " + cookieService.getUserId() + "\n"; 
		cookies += "cookieMonth: " + cookieService.getCookieMonth() + "\n";
		cookies += "hasSuggested: " + cookieService.getHasSuggested() + "\n";
		cookies += "numOfVotes: " + cookieService.getNumOfVotes();
		model.addAttribute("msg", cookies);
		return "ErrorPage";
	}
	
	/*
	 * Reset cookie 
	 */
	@RequestMapping(value = "/reset", method = RequestMethod.GET)
	public String resetCookie(Model model, HttpServletRequest request, HttpServletResponse response) {
		logger.debug("reset cookie()");
		cookieService.setHttpServletRequest(request);
		cookieService.setHttpServletResponse(response);
		cookieService.resetCookies();
		Cookie cookie = new Cookie("cookieMonth", "201601");
		cookieService.setCookie(cookie);
		cookie = new Cookie("userId", "");
		cookieService.setCookie(cookie);
		return "redirect:/cookie";
	}
	
	@ExceptionHandler(EmptyResultDataAccessException.class)
	public ModelAndView handleEmptyData(HttpServletRequest req, Exception ex) {
		logger.debug("handleEmptyData()");
		logger.error("Request: {}, error ", req.getRequestURL(), ex);
		ModelAndView model = new ModelAndView();
		model.setViewName("error");
		model.addObject("msg", "Error");
		return model;

	}


}