package com.doorgga.service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface CookieService {
	public void setHttpServletRequest(HttpServletRequest request);

	public void setHttpServletResponse(HttpServletResponse response);

	public void setCookie(Cookie cookie);

	public Cookie getCookie(String key);

	public int getNumOfVotes();

	public void decreaseNumOfVotes();

	public void resetCookies();

	public boolean getHasSuggested();

	public void setHasSuggested(boolean hasSuggested);

	public String getCookieMonth();

	public String setCookieMonth();
	
	public String getUserId();
	
	public String setUserId();
}
