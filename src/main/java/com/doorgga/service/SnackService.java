package com.doorgga.service;

import java.util.List;

import com.doorgga.domain.Snack;

public interface SnackService {

	public boolean updateSnackVote(List<Snack> snacks, Snack snack);

	public void updateSnackVotes(List<Snack> snacks);

	public void updateSnackHasSuggested(List<Snack> snacks, Snack snack);

	public List<Snack> getTopNSnacks(List<Snack> snacks, int n);

	public List<Snack> getAllSnacks();

	public Snack getNewSuggestedSnack(Snack snack);

	public List<Snack> getSuggestedSnacks(List<Snack> snacks, List<Integer> votedSnackIds);

	public List<Snack> getOptionalSnacks(List<Snack> snacks);

	public int getSnackVote(int id);
	
	public boolean insertVoteDetail(String userId, Snack snack);
	
	public List<Integer> getVotedSnackIds(String userId);

}
