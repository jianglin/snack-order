package com.doorgga.service;

import java.util.Calendar;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

@Service("CookieService")
public class CookieServiceImp implements CookieService {
	private HttpServletRequest request;
	private HttpServletResponse response;

	/*
	 * Create cookie if not exits, otherwise update cookie value
	 * 
	 *  @param Cookie needs to be set
	 */
	public void setCookie(Cookie cookie) {
		String key = cookie.getName();
		String value = cookie.getValue();
		Cookie c = getCookie(key);
		if (c == null) {
			c = new Cookie(key, value);
			c.setPath("/");
			c.setMaxAge(3600 * 24 * 60);
			response.addCookie(c);
		} else {
			c.setValue(value);
			response.addCookie(c);
		}
	}

	/*
	 * Get Cookie by key
	 * 
	 * @param String key
	 * @return Cookie with given key
	 */
	public Cookie getCookie(String key) {
		Cookie[] cookies = request.getCookies();
		Cookie c = null;
		if (cookies != null) {
			int i = 0;
			boolean cookieExists = false;
			while (!cookieExists && i < cookies.length) {
				if (cookies[i].getName().equals(key)) {
					cookieExists = true;
					c = cookies[i];
					c.setPath("/");
					c.setMaxAge(3600 * 24 * 60);
				} else {
					i++;
				}
			}
		}
		
		return c;
	}

	/*
	 * Set http servlet request
	 * 
	 * @param HttpServletRequest
	 */
	public void setHttpServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	/*
	 * Set http servlet reponse
	 * 
	 * @param HttpServletResponse
	 */
	public void setHttpServletResponse(HttpServletResponse response) {
		this.response = response;

	}

	/*
	 * Get num of votes from cookie
	 * 
	 * @return int num of votes remaining
	 */
	public int getNumOfVotes() {
		Cookie voteCookie = getCookie("numOfVotes");
		if (voteCookie == null) {
			setCookie(new Cookie("numOfVotes", "3"));
			return 3;
		} else {
			return Integer.parseInt(voteCookie.getValue());
		}

	}

	/*
	 * Decrease number of votes by 1
	 */
	public void decreaseNumOfVotes() {
		int currentNumOfVotes = Integer.parseInt(getCookie("numOfVotes").getValue());
		currentNumOfVotes--;
		setCookie(new Cookie("numOfVotes", "" + currentNumOfVotes));
	}

	/*
	 * Get hasSuggested value from cookie
	 * 
	 * @return boolean true if a suggestion has been made
	 */
	public boolean getHasSuggested() {
		Cookie suggestCookie = getCookie("hasSuggested");
		if (suggestCookie == null) {
			setHasSuggested(false);
			return false;
		} else if (suggestCookie.getValue().equals("FALSE")) {
			return false;
		}
		return true;
	}

	/*
	 * Set hasSuggested
	 * 
	 * @param boolean hasSuggested indicates if a suggestion has been made
	 */
	public void setHasSuggested(boolean hasSuggested) {
		String value = "FALSE";
		if (hasSuggested) {
			value = "TRUE";
		}
		setCookie(new Cookie("hasSuggested", value));
	}

	/*
	 * Get cookie month.  cookieMonth is used to check if user has made a suggestion and number of votes
	 * 
	 * @return String cookieMonth which is a unique identifier for each month, eg. 201611
	 */
	public String getCookieMonth() {
		Cookie monthCookie = getCookie("cookieMonth");
		if (monthCookie == null) {
			return setCookieMonth();
		}
		else {
			return monthCookie.getValue();
		}
	}

	/*
	 * Set a cookie to store current month info in foramt yyyymm
	 * 
	 * @return String cookieMonth
	 */
	public String setCookieMonth() {
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		String value = "" + year + month;
		setCookie(new Cookie("cookieMonth", value));
		return value;
	}

	/*
	 * Reset numOfVotes and hasSuggested cookie if cookie in cookieMonth is different from server month.
	 */
	public void resetCookies() {
		if (getHasSuggested()) {
			setHasSuggested(false);
		}
		if (getNumOfVotes() !=3) {
			setCookie(new Cookie("numOfVotes", "3"));
		};
	}
	
	/*
	 * Get user id
	 * 
	 * @return String user id
	 */
	public String getUserId() {
		Cookie userId = getCookie("userId");
		if (userId == null) {
			return ""; 
		}
		else {
			return userId.getValue();
		}
	}

	/*
	 * Set user id
	 * 
	 * @return String user id that has been set
	 */
	public String setUserId() {
		String id = UUID.randomUUID().toString();
		setCookie(new Cookie("userId", id));
		return id;
	}
}
