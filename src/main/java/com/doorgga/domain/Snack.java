package com.doorgga.domain;

public class Snack {
	
	private int id;
	private String name;
	private boolean optional;
	private String purchaseLocations;
	private int purchaseCount;
	private String lastPurchaseDate;
	private int vote;
	private boolean hasSuggested;
	private boolean hasVoted;
	
	public Snack() {
		this.id = -1;
		this.vote = 0;
		this.hasSuggested = false;
		this.hasVoted = false;
	};

	public Snack(int id) {
		this.id = id;
		this.vote = 0;
		this.hasSuggested = false;
		this.hasVoted = false;
	}
	public Snack(int id, int vote) {
		this.id = id;
		this.vote = vote;
		this.hasSuggested = false;
		this.hasVoted = false;
	}

	public Snack(int id, boolean hasSuggested) {
		this.id = id;
		this.hasSuggested = hasSuggested;
	}

	public Snack(int id, String name, boolean optional, String purchaseLocations, int purchaseCount,
			String lastPurchaseDate) {
		this.id = id;
		this.name = name;
		this.optional = optional;
		this.purchaseLocations = purchaseLocations;
		this.purchaseCount = purchaseCount;
		this.lastPurchaseDate = lastPurchaseDate;
	}

	public int getId() {
		return this.id;
	}

	public int getVote() {
		return this.vote;
	}

	public boolean getHasSuggested() {
		return this.hasSuggested;
	}
	
	public boolean getHasVoted() {
		return this.hasVoted;
	}
	public String getName() {
		return this.name;
	}

	public boolean getOptional() {
		return this.optional;
	}

	public int getPurchaseCount() {
		return this.purchaseCount;
	}

	public String getPurchaseLocations() {
		return this.purchaseLocations;
	}

	public String getLastPurchaseDate() {
		return this.lastPurchaseDate;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPurchaseLocations(String purchaseLocations) {
		this.purchaseLocations = purchaseLocations;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

	public void setPurchaseCount(int purchaseCount) {
		this.purchaseCount = purchaseCount;
	}
	
	public void setLastPurchaseDate(String lastPurchaseDate) {
		this.lastPurchaseDate = lastPurchaseDate; 
	}
	
	public void setVote(int vote) {
		this.vote = vote;
	}

	public void setHasSuggested(boolean hasSuggested) {
		this.hasSuggested = hasSuggested;
	}
	
	public void setHasVoted(boolean hasVoted) {
		this.hasVoted = hasVoted;
	}

}
