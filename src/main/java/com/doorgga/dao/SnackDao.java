package com.doorgga.dao;

import java.util.List;

import com.doorgga.domain.Snack;

public interface SnackDao {
	public int insertVoteData(Snack snack);

	public int updateVoteData(Snack snack);

	public boolean insertVoteDetailData(String userId, Snack snack);
	
	public int findVoteDataByID(int id);

	public List<Snack> getTopNSnacks(int n);
	
	public List<Integer> getVotedSnackIds(String userId);
	
}
