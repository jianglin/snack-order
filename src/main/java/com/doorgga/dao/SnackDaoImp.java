package com.doorgga.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.doorgga.domain.Snack;

@Repository
public class SnackDaoImp implements SnackDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate)
			throws DataAccessException {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	/*
	 * Get top n snacks order by votes
	 * 
	 * @param int n
	 * @return List<Snack> list of snacks with id and votes info
	 */
	public List<Snack> getTopNSnacks(int n) {

		String sql = "SELECT * FROM Votes ORDER BY Vote DESC";
		return namedParameterJdbcTemplate.query(sql, new SnackMapper());

	}

	/*
	 * Find vote data by snack id from storage
	 * 
	 * @param int id snack id
	 * @return int votes for given snack
	 */
	public int findVoteDataByID(int id) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);

		String sql = "SELECT * FROM Votes WHERE snack_id=:id";

		Snack snack = null;
		try {
			snack = namedParameterJdbcTemplate.queryForObject(sql, params, new SnackMapper());
		} catch (EmptyResultDataAccessException e) {
			return -1;
		}
		return snack.getVote();

	}

	/*
	 * Insert vote new snack vote data to db. Newly inserted vote to be 0.
	 * 
	 * @param Snack snack which contains the snack id
	 * @return number of row affected, it should be 1 if no error
	 */
	public int insertVoteData(Snack snack) {
		String sql = "INSERT INTO Votes (snack_id, vote) VALUES ( :id, 0)";
		return namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(snack));
	}

	/*
	 * Update vote data for given Snack
	 * 
	 * @param Snack snack vote need to be updated
	 * @return int number of rows affected, should return 1 if no error
	 */
	public int updateVoteData(Snack snack) {
		String sql = "UPDATE Votes SET vote = vote + 1 WHERE snack_id = :id";
		return namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(snack));

	}

	
	/*
	 * Insert record into Votes_Detail table
	 * 
	 * @param String unique user id
	 * @param String month cookie month
	 * @param Snack snack to be inserted
	 * @return boolean true if insert successfully
	 */
	public boolean insertVoteDetailData(String userId, Snack snack) {
		String sql = "INSERT INTO Votes_Detail (user_id, snack_id) VALUES ( :userId, :snackId)";
		Map<String, String> params = new HashMap<String, String>();
		params.put("userId", userId);
		params.put("snackId", ""+ snack.getId());
		return namedParameterJdbcTemplate.update(sql, params) == 1;
	}

	/*
	 * Get List of snack id given user and month
	 * 
	 * @param String unique user id
	 * @param String month cookie month
	 * @return List of Snack IDs that has been voted by given 'userId' and 'month'
	 */
	public List<Integer> getVotedSnackIds(String userId) {
		String sql = "SELECT snack_id FROM Votes_Detail WHERE user_id = (:userId)";
		Map<String, String> params = new HashMap<String, String>();
		params.put("userId", userId);
		return namedParameterJdbcTemplate.query(sql, params, new RowMapper<Integer>(){
            public Integer mapRow(ResultSet rs, int rowNum) 
                    throws SQLException {
            		return rs.getInt(1);
            }
			});
	}
	
	/*
	 * Binding Snack object variable with sql parameter 
	 * 
	 * @param Snack contains id and vote data
	 * @return MapSqlParameterSource
	 */
	private SqlParameterSource getSqlParameterByModel(Snack snack) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", snack.getId());
		paramSource.addValue("vote", snack.getVote());
		return paramSource;
	}

	/*
	 * Map sql results to Snack object 
	 * 
	 */
	private static final class SnackMapper implements RowMapper<Snack> {
		public Snack mapRow(ResultSet rs, int rowNum) throws SQLException {
			Snack snack = new Snack();
			snack.setId(rs.getInt("snack_id"));
			snack.setVote(rs.getInt("vote"));
			return snack;
		}
	}






}