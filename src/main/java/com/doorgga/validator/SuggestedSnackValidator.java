package com.doorgga.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.doorgga.domain.Snack;

@Component
public class SuggestedSnackValidator implements Validator {
	
	public boolean supports(Class<?> clazz) {
		return Snack.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {

		Snack snack = (Snack) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "NotEmpty.suggestedsnack.id");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.suggestedsnack.name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "purchaseLocations", "NotEmpty.suggestedsnack.purchaseLocations");
		
		int nameLength = snack.getName().length();
		if( nameLength < 0 || nameLength > 200) {
			errors.rejectValue("name", "InvalidLength.suggestedsnack.name");
		}
		
		int locationLength = snack.getPurchaseLocations().length();
		if( locationLength < 0 || locationLength > 50) {
			errors.rejectValue("purchaseLocations", "InvalidLength.suggestedsnack.purchaseLocations");
		}
	
	}
}
