<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!doctype html>
<html class="no-js" lang="en-us">
<head>
<!-- META DATA -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!--[if IE]><meta http-equiv="cleartype" content="on" /><![endif]-->
<!-- SEO -->
<title>Snack Food Ordering System</title>
<!-- ICONS -->
<link rel="apple-touch-icon" sizes="57x57"
	href="assets/media/images/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60"
	href="assets/media/images/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72"
	href="assets/media/images/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="assets/media/images/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114"
	href="assets/media/images/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="assets/media/images/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144"
	href="assets/media/images/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="assets/media/images/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180"
	href="assets/media/images/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"
	href="assets/media/images/favicon/favicon-192x192.png">
<link rel="icon" type="image/png" sizes="160x160"
	href="assets/media/images/favicon/favicon-160x160.png">
<link rel="icon" type="image/png" sizes="96x96"
	href="assets/media/images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="32x32"
	href="assets/media/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16"
	href="assets/media/images/favicon/favicon-16x16.png">
<meta name="msapplication-TileImage"
	content="assets/media/images/favicon/mstile-144x144.png">
<meta name="msapplication-TileColor" content="#ff0000">
<!-- STYLESHEETS -->
<link rel="stylesheet" media="screen, projection"
	href="assets/styles/modern.css" />
</head>
<body>
	<div class="masthead" role="banner">
		<div class="masthead-hd">
			<h1 class="hdg hdg_1 mix-hdg_extraBold">
				<a href="index.html">SnaFoo</a>
			</h1>
			<p class="masthead-hd-sub">Snack Food Ordering System</p>
		</div>
		<div class="masthead-nav" role="navigation">
			<ul>
				<spring:url value="/voting" var="voting" />
				<li><a href='${voting}'>Voting</a></li>
				<spring:url value="/suggestions" var="suggestions" />
				<li><a href='${suggestions}'>Suggestions</a></li>
				<spring:url value="/shoppinglist" var="shoppinglist" />
				<li><a href='${shoppinglist}'>Shopping List</a></li>
			</ul>
		</div>
	</div>
	<div class="wrapper">
		<div class="content" role="main">
			<div class="shelf shelf_5">
				<h2 class="hdg hdg_1">Shopping List</h2>
			</div>
			<div class="shelf shelf_1">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">Snack Name</th>
							<th scope="col">Purchase Location</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="Snack" items="${shoppingSnackList}">
							<tr>
								<td>${Snack.name}</td>
								<td>${Snack.purchaseLocations}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<!-- /content -->
	</div>
	<!-- /wrapper -->
</body>
</html>
