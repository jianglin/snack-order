<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!doctype html>
<html class="no-js" lang="en-us">
<head>
<!-- META DATA -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!--[if IE]><meta http-equiv="cleartype" content="on" /><![endif]-->
<!-- SEO -->
<title>Snack Food Ordering System</title>
<!-- ICONS -->
<link rel="apple-touch-icon" sizes="57x57"
	href="assets/media/images/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60"
	href="assets/media/images/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72"
	href="assets/media/images/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="assets/media/images/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114"
	href="assets/media/images/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="assets/media/images/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144"
	href="assets/media/images/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="assets/media/images/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180"
	href="assets/media/images/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"
	href="assets/media/images/favicon/favicon-192x192.png">
<link rel="icon" type="image/png" sizes="160x160"
	href="assets/media/images/favicon/favicon-160x160.png">
<link rel="icon" type="image/png" sizes="96x96"
	href="assets/media/images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="32x32"
	href="assets/media/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16"
	href="assets/media/images/favicon/favicon-16x16.png">
<meta name="msapplication-TileImage"
	content="assets/media/images/favicon/mstile-144x144.png">
<meta name="msapplication-TileColor" content="#ff0000">
<!-- STYLESHEETS -->
<link rel="stylesheet" media="screen, projection"
	href="assets/styles/modern.css" />
</head>
<body>
	<div class="masthead" role="banner">
		<div class="masthead-hd">
			<h1 class="hdg hdg_1 mix-hdg_extraBold">
				<a href="index.html">SnaFoo</a>
			</h1>
			<p class="masthead-hd-sub">Snack Food Ordering System</p>
		</div>
		<div class="masthead-nav" role="navigation">
			<ul>
				<spring:url value="/voting" var="voting" />
				<li><a href='${voting}'>Voting</a></li>
				<spring:url value="/suggestions" var="suggestions" />
				<li><a href='${suggestions}'>Suggestions</a></li>
				<spring:url value="/shoppinglist" var="shoppinglist" />
				<li><a href='${shoppinglist}'>Shopping List</a></li>
			</ul>
		</div>
	</div>
	<div class="wrapper">
		<div class="content" role="main">
			<div class="shelf shelf_5">
				<h1 class="hdg hdg_1">Voting</h1>
			</div>
			<div class="shelf shelf_2">
				<p>You are able to vote for up to three selections each month.</p>
			</div>
			<div class="shelf shelf_2">
				<div class="voteBox">
					<div class="voteBox-hd">
						<h2 class="hdg hdg_3">Votes Remaining</h2>
					</div>
					<div class="voteBox-body">
						<p class="counter counter_green ${hidden3}">3</p>
						<p class="counter counter_yellow ${hidden2}">2</p>
						<p class="counter counter_red ${hidden1}">1</p>
					</div>
				</div>
			</div>
			<div class="shelf shelf_2">
				<p class="error ${maxVoteReached}">
					Opps! You have already voted the total allowed times this month.<br />Come
					back next month to vote again!
				</p>
			</div>
			<div class="split">
				<div class="shelf shelf_2">
					<div class="shelf">
						<h2 class="hdg hdg_2 mix-hdg_centered ">Snacks Always
							Purchased</h2>
					</div>

					<ul class="list list_centered">
						<c:if test="${not empty allwaysPurchasedSnackList}">

							<c:forEach var="Snack" items="${allwaysPurchasedSnackList}">
								<li>${Snack.name}</li>
							</c:forEach>

						</c:if>
					</ul>
				</div>
			</div>
			<div class="split">
				<div class="shelf shelf_2">
					<div class="shelf">
						<h2 class="hdg hdg_2 mix-hdg_centered ">Snacks suggested this
							month</h2>
					</div>
					<div class="shelf shelf_5">
						<table class="table">
							<thead>
								<tr>
									<th scope="col">Snack Food</th>
									<th scope="col">Current Votes</th>
									<th scope="col">VOTE</th>
									<th scope="col">Last Date Purchased</th>
								</tr>
							</thead>
							<tbody>
								<c:if test="${not empty suggestedSnackList}">
									<c:forEach var="Snack" items="${suggestedSnackList}">
										<tr>
											<td>${Snack.name}</td>
											<td>${Snack.vote}</td>
											<td><spring:url value="/voting/${Snack.id}"
													var="voteForSnack" /> <form:form action='${voteForSnack}'
													method="POST">
													<button ${checkboxDisabled} ${Snack.hasVoted ? 'disabled' : ''} class="btn btn_clear" type="submit"
														value="${Snack.id}" ${voteCheckboxDisabled}>
														<i class="icon-check ${Snack.hasVoted ? 'icon-check_voted' : 'icon-check_noVote'}"></i>
													</button>
												</form:form></td>
											<td>${Snack.lastPurchaseDate}</td>
										</tr>
									</c:forEach>
								</c:if>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /content -->
	</div>
	<!-- /wrapper -->
</body>
</html>
