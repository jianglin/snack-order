DROP TABLE Votes IF EXISTS;

CREATE TABLE Votes (
  snack_id INTEGER NOT NULL,
  vote INTEGER NOT NULL,
  PRIMARY KEY (snack_id)
);

DROP TABLE Votes_Detail IF EXISTS;

CREATE TABLE Votes_Detail (
   user_id VARCHAR(50) NOT NULL,
   snack_id INTEGER NOT NULL,
   PRIMARY KEY(user_id, snack_id),
   FOREIGN KEY(snack_id) REFERENCES Votes(snack_id),
);